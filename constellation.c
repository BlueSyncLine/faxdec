#include <math.h>
#include <complex.h>
#include "constellation.h"
#include "util.h"
//#include "log.h"

// Find the closest symbol.
void constellation_decide(const constellation_t *constellation, constellation_decision_t *decision, double complex symbol) {
    int i, bestIndex;
    double complex point, bestPoint;
    double distance, bestDistance;
    constellation_cell_t cell;

    bestIndex = -1;
    for (i = 0; i < constellation->length; i++) {
        cell = constellation->cells[i];
        point = cell.x + I * cell.y;
        distance = cabs(symbol - point);

        if (bestIndex == -1 || distance < bestDistance) {
            bestPoint = point;
            bestDistance = distance;
            bestIndex = i;
        }
    }

    decision->originalCoords = symbol;
    decision->symbol.coords = bestPoint;
    decision->symbol.value = bestIndex;
    decision->amplitudeError = cabs(symbol) / cabs(bestPoint);
    decision->phaseError = carg(symbol * conj(bestPoint));
    //log_printf("constellation: %f %f -> %f %f\n", creal(symbol), cimag(symbol), creal(decision->symbol.coords), cimag(decision->symbol.coords));
}

// Simply extract the coordinates.
double complex constellation_closest(const constellation_t *constellation, double complex symbol) {
    constellation_decision_t decision;
    constellation_decide(constellation, &decision, symbol);
    return decision.symbol.coords;
}

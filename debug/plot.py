from matplotlib import pyplot as plt

xs = []
ys = []
txs = []
tys = []
amperr = []
phaerr = []
step = []
amplitude = []
trlerr = []
with open("debug/symbols.txt") as infile:
    for line in infile:
        line = line.strip().split()

        if line[0] == "data":
            xs.append(float(line[1]))
            ys.append(float(line[2]))
            amperr.append(float(line[3]))
            phaerr.append(float(line[4]))
        elif line[0] == "training":
            txs.append(float(line[1]))
            tys.append(float(line[2]))
        elif line[0] == "amplitude":
            amplitude.append(float(line[1]))
        elif line[0] == "step":
            step.append(int(line[1]))
        elif line[0] == "trellis":
            trlerr.append(float(line[1]))

plt.figure("I/Q")
plt.plot(xs)
plt.plot(ys)
plt.figure("Training")
plt.hist2d(txs, tys, 128, [[-10, 10], [-10, 10]])
plt.figure("Training (bridge)")
plt.hist2d(txs[-64:], tys[-64:], 128, [[-10, 10], [-10, 10]])
plt.figure("Data")
plt.hist2d(xs[:3500], ys[:3500], 128, [[-10, 10], [-10, 10]])
plt.figure("Amplitude")
plt.plot(amplitude)
plt.figure("Error signals")
plt.plot(amperr)
plt.plot(phaerr)
plt.figure("Sample step")
plt.plot(step)
plt.figure("Trellis error distance")
plt.plot(trlerr)
plt.show()

#ifndef T30_H
#define T30_H

// T.30 parameters.

// FCF top nibbles.
enum {
    T30_FCF_INIT = 0x0,
    T30_FCF_COMMAND = 0x8,
    T30_FCF_RECEIVE_1 = 0x4,
    T30_FCF_RECEIVE_2 = 0xc,
    T30_FCF_PREMSG_1 = 0x2,
    T30_FCF_PREMSG_2 = 0xa,
    T30_FCF_POSTMSG_1 = 0x7,
    T30_FCF_POSTMSG_2 = 0xf,
    T30_FCF_POSTRES_1 = 0x3,
    T30_FCF_POSTRES_2 = 0xb,
    T30_FCF_OTHER_1 = 0x5,
    T30_FCF_OTHER_2 = 0xd
};

// FCF lower nibbles.
enum {
    T30_INIT_DIS = 0x1,
    T30_INIT_CSI = 0x2,
    T30_INIT_NSF = 0x4
};

enum {
    T30_COMMAND_DTC = 0x1,
    T30_COMMAND_CIG = 0x2,
    T30_COMMAND_NSC = 0x4,
    T30_COMMAND_PWD = 0x3,
    T30_COMMAND_SEP = 0x5
};

enum {
    T30_RECEIVE_DCS = 0x1,
    T30_RECEIVE_TSI = 0x2,
    T30_RECEIVE_NSS = 0x4,
    T30_RECEIVE_SUB = 0x3,
    T30_RECEIVE_PWD = 0x6
};

enum {
    T30_PREMSG_CFR = 0x1,
    T30_PREMSG_FTT = 0x2
};

enum {
    T30_POSTMSG_EOM = 0x1,
    T30_POSTMSG_MPS = 0x2,
    T30_POSTMSG_EOP = 0x4,
    T30_POSTMSG_PRI_EOM = 0x9,
    T30_POSTMSG_PRI_MPS = 0xa,
    T30_POSTMSG_PRI_EOP = 0xc
};

enum {
    T30_POSTRES_MCF = 0x1,
    T30_POSTRES_REP = 0x3,
    T30_POSTRES_REN = 0x2,
    T30_POSTRES_PIP = 0x5,
    T30_POSTRES_PIN = 0x4,
    T30_POSTRES_FDM = 0xf
};

enum {
    T30_OTHER_DCN = 0xf,
    T30_OTHER_CRP = 0x8
};

#define T30_DCS_MIN_LENGTH  3
#define T30_DCS_RATE_V17_14K 1
#define T30_DCS_RATE_V17_12K 5
#define T30_DCS_RATE_V17_9600 9
#define T30_DCS_RATE_V17_7200 13
#endif

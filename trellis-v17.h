#ifndef V17_TRELLIS_H
#define V17_TRELLIS_H

#include "constellation.h"

// TODO is this the correct choice for the code rate?
#define V17_TRELLIS_TRUNCATION 32

// Eight states.
#define V17_TRELLIS_STATES 8

// Constellation types for neighbor searching.
enum {
    V17_TRELLIS_CONSTELLATION_16,
    V17_TRELLIS_CONSTELLATION_32,
    V17_TRELLIS_CONSTELLATION_64,
    V17_TRELLIS_CONSTELLATION_128,
};

typedef struct {
    int symbol;
    int prevState;
    double pathMetric;
} v17_trellis_entry_t;

typedef struct {
    v17_trellis_entry_t *trellis;
    int currentIndex;
    int skipCounter;

    const constellation_t *constellation;
    const int (*neighbors)[2];
} v17_trellis_decoder_t;

int v17_trellis_parity(int currentState, int symbol);
int v17_trellis_advance(int currentState, int symbol);

int v17_trellis_decoder_input(v17_trellis_decoder_t *decoder, const constellation_decision_t *decision);

void v17_trellis_decoder_reset(v17_trellis_decoder_t *decoder);
int v17_trellis_decoder_alloc(v17_trellis_decoder_t *decoder, const constellation_t *constellation, int constellationType);
void v17_trellis_decoder_free(v17_trellis_decoder_t *decoder);

#endif

#ifndef HDLC_H
#define HDLC_H

// FCS polynomial.
#define HDLC_FCS_POLY 0x1021

// Valid remainder.
#define HDLC_FCS_VALID 0x1d0f

// Error codes.
enum {
    HDLC_INVALID_FCS = -1,
    HDLC_OVERFLOW_DISCARD = -2,
    HDLC_INVALID_FLAG = -3
};

typedef struct {
    unsigned char *frame;
    int bufsize;
    unsigned char currentByte;
    int consecutiveOnes;
    int awaitingSeventh;
    int bitIndex;
    int frameIndex;
} hdlc_decoder_t;

int hdlc_check_fcs(unsigned char *buf, int n);
int hdlc_decoder_alloc(hdlc_decoder_t *decoder, int bufsize);
int hdlc_decoder_input(hdlc_decoder_t *decoder, int bit);
void hdlc_decoder_free(hdlc_decoder_t *decoder);

#endif

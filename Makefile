all: clean testdemod

clean:
	rm -f *.o testdemod

testdemod:
	gcc -Wall -Wextra -g -lm *.c -o testdemod

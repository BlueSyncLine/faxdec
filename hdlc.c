#include <malloc.h>
#include "hdlc.h"
#include "log.h"

// Checks the HDLC checksum of a frame.
// A non-zero value is returned if it's correct.
int hdlc_check_fcs(unsigned char *buf, int n) {
    int i, j, x, bit;
    unsigned char byte;

    x = 0xffff;

    for (i = 0; i < n; i++) {
        byte = buf[i];

        // Feed the bits in MSB-first.
        for (j = 0; j < 8; j++) {
            bit = (byte & 0x80) >> 7;
            byte <<= 1;

            x <<= 1;
            x ^= bit << 16;

            // Carry?
            if (x & 0x10000) {
                x &= 0xffff;
                x ^= HDLC_FCS_POLY;
            }
        }
    }

    // The remainder should be this value for a valid FCS.
    return x == HDLC_FCS_VALID;
}

// Initialize the decoder, returns 0 on failure.
int hdlc_decoder_alloc(hdlc_decoder_t *decoder, int bufsize) {
    decoder->bufsize = bufsize;
    decoder->currentByte = 0;
    decoder->consecutiveOnes = 0;
    decoder->awaitingSeventh = 0;

    // Negative means that we're only interested in the flag byte.
    decoder->bitIndex = -1;
    decoder->frameIndex = 0;

    if (!(decoder->frame = malloc(bufsize)))
        return 0;

    return 1;
}

// Feeds a bit into the decoder.
// Returns:
// A positive integer with the length if there's a frame in the buffer.
// 0 if there's no data.
// Negative return values signify an error.
int hdlc_decoder_input(hdlc_decoder_t *decoder, int bit) {
    int length;
    unsigned char byte;

    // Allow for direct pass-through from a receiver.
    if (bit == -1)
        return 0;

    //log_printf("[%d] ", bit);

    // If we are not waiting for a seventh bit...
    if (!decoder->awaitingSeventh) {
        // Five consecutive ones in a row...
        if (decoder->consecutiveOnes == 5) {
            // Reset the counter.
            decoder->consecutiveOnes = 0;

            // If the sixth is a zero bit, it's inserted as stuffing, discard.
            if (!bit)
                return 0;

            // If it's an one, then await the seventh bit.
            decoder->awaitingSeventh = 1;
            return 0;
        }
    } else {
        // This is the seventh bit.
        decoder->awaitingSeventh = 0;

        // Preserve the current frame index.
        length = decoder->frameIndex;

        // Reset.
        decoder->currentByte = 0;
        decoder->frameIndex = 0;

        // A correct flag byte?
        if (!bit) {
            // A data byte shall follow.
            decoder->bitIndex = 0;

            // Do we have a frame?
            if (length > 4) {
                // If the FCS is valid, return the frame length.
                if (hdlc_check_fcs(decoder->frame, length)) {
                    log_printf("HDLC: frame (len=%d, final=%d)\n", length, (decoder->frame[1] & 8) >> 3);
                    log_printHex(decoder->frame, length);
                    return length;
                }

                //log_printf("HDLC: FCS error\n");

                // FCS error.
                return HDLC_INVALID_FCS;
            } else {
                //log_printf("HDLC: frame start\n");
                return 0;
            }
        } else {
            // Nope, report protocol error and resync.
            decoder->bitIndex = -1;
            return HDLC_INVALID_FLAG;
        }
    }

    // If this bit is an one, increment the consecutive ones counter.
    // Reset it otherwise.
    decoder->consecutiveOnes = bit ? decoder->consecutiveOnes + 1 : 0;

    // We only want the flag sequence. Who needs this bit?
    if (decoder->bitIndex == -1) {
        return 0;
    } else {
        // Shift in the current bit (MSB-first).
        decoder->currentByte <<= 1;
        decoder->currentByte |= bit;

        // Have we accumulated eight bits?
        if (++decoder->bitIndex == 8) {
            // Yes.
            decoder->bitIndex = 0;
            byte = decoder->currentByte;
            decoder->currentByte = 0;

            // Can't write!
            if (decoder->frameIndex == decoder->bufsize) {
                // Skip further bits, await resync for the next frame.
                decoder->bitIndex = -1;
                decoder->frameIndex = 0;
                return HDLC_OVERFLOW_DISCARD;
            }

            // Write the byte and increment the index.
            //log_printf("HDLC: byte %02x (idx=%d)\n", byte, decoder->frameIndex);
            decoder->frame[decoder->frameIndex++] = byte;
        }

        // No data yet.
        return 0;
    }
}

// Deallocate the buffer memory.
void hdlc_decoder_free(hdlc_decoder_t *decoder) {
    free(decoder->frame);
}

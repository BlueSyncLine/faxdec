#ifndef CONSTELLATION_H
#define CONSTELLATION_H

#include <complex.h>

typedef struct constellation_cell {
    int x;
    int y;
} constellation_cell_t;

typedef struct {
    double complex coords;
    int value;
} constellation_symbol_t;

typedef struct {
    constellation_symbol_t symbol;
    double amplitudeError;
    double phaseError;
    double complex originalCoords;
} constellation_decision_t;

typedef struct constellation {
    int length;
    const struct constellation_cell cells[];
} constellation_t;
void constellation_decide(const constellation_t *constellation, constellation_decision_t *decision, double complex symbol);
double complex constellation_closest(const constellation_t *constellation, double complex symbol);
#endif

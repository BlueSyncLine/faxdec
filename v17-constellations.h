const constellation_t V17_CONSTELLATION_SYNC = {
    .cells={
        {-6, -2}, {2, -6}, {6, 2}, {-2, 6},
    }, .length=4
};

const constellation_t V17_CONSTELLATION_DATA_16 = {
    .cells={
        {6, -6}, {-2, 6}, {6, 2}, {-6, -6}, {-6, 6},
        {2, -6}, {-6, -2}, {6, 6}, {-2, 2}, {6, -2},
        {-2, -6}, {2, 2}, {2, -2}, {-6, 2}, {2, 6},
        {-2, -2},
    }, .length=16
};

const constellation_t V17_CONSTELLATION_DATA_32 = {
    .cells={
        {-8, 2}, {-6, -4}, {-4, 6}, {2, 8}, {8, -2},
        {6, 4}, {4, -6}, {-2, -8}, {0, 2}, {-6, 4},
        {4, 6}, {2, 0}, {0, -2}, {6, -4}, {-4, -6},
        {-2, 0}, {0, -6}, {2, -4}, {-4, -2}, {-6, 0},
        {0, 6}, {-2, 4}, {4, 2}, {6, 0}, {8, 2},
        {2, 4}, {4, -2}, {2, -8}, {-8, -2}, {-2, -4},
        {-4, 2}, {-2, 8},
    }, .length=32
};

const constellation_t V17_CONSTELLATION_DATA_64 = {
    .cells={
        {7, 1}, {-5, -1}, {-1, 5}, {1, -7}, {-7, -1},
        {5, 1}, {1, -5}, {-1, 7}, {3, -3}, {-1, 3},
        {3, 1}, {-3, -3}, {-3, 3}, {1, -3}, {-3, -1},
        {3, 3}, {7, -7}, {-5, 7}, {7, 5}, {-7, -7},
        {-7, 7}, {5, -7}, {-7, -5}, {7, 7}, {-1, -7},
        {3, 7}, {7, -3}, {-7, 1}, {1, 7}, {-3, -7},
        {-7, 3}, {7, -1}, {3, 5}, {-1, -5}, {-5, 1},
        {5, -3}, {-3, -5}, {1, 5}, {5, -1}, {-5, 3},
        {-1, 1}, {3, -1}, {-1, -3}, {1, 1}, {1, -1},
        {-3, 1}, {1, 3}, {-1, -1}, {-5, 5}, {7, -5},
        {-5, -7}, {5, 5}, {5, -5}, {-7, 5}, {5, 7},
        {-5, -5}, {-5, -3}, {7, 3}, {3, -7}, {-3, 5},
        {5, 3}, {-7, -3}, {-3, 7}, {3, -5},
    }, .length=64
};

const constellation_t V17_CONSTELLATION_DATA_128 = {
    .cells={
        {-8, -3}, {9, 2}, {2, -9}, {-3, 8}, {8, 3},
        {-9, -2}, {-2, 9}, {3, -8}, {-8, 1}, {9, -2},
        {-2, -9}, {1, 8}, {8, -1}, {-9, 2}, {2, 9},
        {-1, -8}, {-4, -3}, {5, 2}, {2, -5}, {-3, 4},
        {4, 3}, {-5, -2}, {-2, 5}, {3, -4}, {-4, 1},
        {5, -2}, {-2, -5}, {1, 4}, {4, -1}, {-5, 2},
        {2, 5}, {-1, -4}, {4, -3}, {-3, 2}, {2, 3},
        {-3, -4}, {-4, 3}, {3, -2}, {-2, -3}, {3, 4},
        {4, 1}, {-3, -2}, {-2, 3}, {1, -4}, {-4, -1},
        {3, 2}, {2, -3}, {-1, 4}, {0, -3}, {1, 2},
        {2, -1}, {-3, 0}, {0, 3}, {-1, -2}, {-2, 1},
        {3, 0}, {0, 1}, {1, -2}, {-2, -1}, {1, 0},
        {0, -1}, {-1, 2}, {2, 1}, {-1, 0}, {8, -3},
        {-7, 2}, {2, 7}, {-3, -8}, {-8, 3}, {7, -2},
        {-2, -7}, {3, 8}, {8, 1}, {-7, -2}, {-2, 7},
        {1, -8}, {-8, -1}, {7, 2}, {2, -7}, {-1, 8},
        {-4, -7}, {5, 6}, {6, -5}, {-7, 4}, {4, 7},
        {-5, -6}, {-6, 5}, {7, -4}, {-4, 5}, {5, -6},
        {-6, -5}, {5, 4}, {4, -5}, {-5, 6}, {6, 5},
        {-5, -4}, {4, -7}, {-3, 6}, {6, 3}, {-7, -4},
        {-4, 7}, {3, -6}, {-6, -3}, {7, 4}, {4, 5},
        {-3, -6}, {-6, 3}, {5, -4}, {-4, -5}, {3, 6},
        {6, -3}, {-5, 4}, {0, -7}, {1, 6}, {6, -1},
        {-7, 0}, {0, 7}, {-1, -6}, {-6, 1}, {7, 0},
        {0, 5}, {1, -6}, {-6, -1}, {5, 0}, {0, -5},
        {-1, 6}, {6, 1}, {-5, 0}
    }, .length=128
};

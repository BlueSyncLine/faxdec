from v17constell import *


points = [[int(p.real), int(p.imag)] for p in CONSTELLATION_DATA_32]
points_per_line = 5
print(len(points))
print("const constellation_t CONSTELLATION = {")
print("    .cells={")
for i in range(0, len(points), points_per_line):
    line = "        "
    for point in points[i: i + points_per_line]:
        line += "{{{}, {}}}, ".format(*point)
    line = line.rstrip()
    print(line)
print("    }}, .length={}".format(len(points)))
print("};")

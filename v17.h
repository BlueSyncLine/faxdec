#ifndef V17_H
#define V17_H

#include <stdint.h>
#include <complex.h>
#include "fir.h"
#include "params.h"
#include "constellation.h"
#include "trellis-v17.h"

#define V17_UPSAMPLING_RATE 12
#define V17_RATE (INPUT_RATE * V17_UPSAMPLING_RATE)
#define V17_BAUD_RATE 2400.0
#define V17_CARRIER_FREQUENCY 1800.0
#define V17_CLOCK_FREQUENCY (V17_BAUD_RATE * 2)

// Beta parameter for the root-raised cosine filter.
// This appears to work better than any other value (0.33, 0.5).
#define V17_RRC_BETA 0.35

// Gardner timing recovery will be invalid over this amplitude.
#define V17_GARDNER_MAX_AMPLITUDE 20.0

// The filter will have 2n + 1 taps.
#define V17_RESAMPLING_FILTER_N 192

// Clock recovery coefficient, chosen empirically.
#define V17_CLOCK_COEFF_SYNC 0.0005
#define V17_CLOCK_COEFF_DATA 0.0001

// AGC coefficients.
#define V17_AGC_COEFF_SYNC 0.1
#define V17_AGC_COEFF_DATA 0.01

// Phase synchronization coefficients.
#define V17_PHASE_COEFF_SYNC 0.005
#define V17_PHASE_COEFF_DATA 0.001

// Chosen empirically.
#define V17_SYNC_RUN_THRESHOLD 128

// Empirically-chosen coefficient.
#define V17_ADAPTEQ_RATE 0.98

// Training durations.
#define V17_LONG_TRAIN_LENGTH 2976
#define V17_SHORT_TRAIN_LENGTH 38
#define V17_BRIDGE_LENGTH 64
#define V17_PREAMBLE_LENGTH 48
#define V17_POSTAMBLE_LENGTH 32
#define V17_POSTAMBLE_THRESHOLD 16

// Scrambler polynomial.
#define V17_SCRAMBLER_POLY (1 | 1 << 18 | 1 << 23)

enum {
    V17_RATE_14K = 6,
    V17_RATE_12K = 5,
    V17_RATE_9600 = 4,
    V17_RATE_7200 = 3
};

enum {
    V17_IDLE = 0,
    V17_ACQUISITION,
    V17_TRAINING,
    V17_DATA
};

typedef struct {
    int dataBitsPerSymbol;
    int retraining;

    const constellation_t *dataConstellation;

    firfilt_t resamplingFilter;

    double complex *equalizerTaps;
    int trainingIndex;
    int preambleCounter;
    int postambleCounter;

    double amplitudeLevel;
    double loPhase;
    double clockPhase;

    int gardnerStrobe;
    double complex gardnerPrev;
    double complex gardnerPrev2;

    int acquisitionStage;
    int syncRun;
    int syncPrev;
    int syncPrevDelta;

    v17_trellis_decoder_t trellis;
    int diffPrev;
    int descramblerState;

    int sampleCounter;
    int lastSymbolTime;
} v17_demod_t;

int v17_demod_alloc(v17_demod_t *demod, int dataBitsPerSymbol);
void v17_demod_startAcquisition(v17_demod_t *demod, int retrain);
int v17_demod_symbol(v17_demod_t *demod, double complex symbol);
int v17_demod_input(v17_demod_t *demod, int16_t intSample);
void v17_demod_free(v17_demod_t *demod);

#endif

#ifndef UTIL_H
#define UTIL_H
#include <complex.h>

int getBit(unsigned char *bytes, int bit);
int getBits(unsigned char *bytes, int start, int end);
int clampInt(int x, int minVal, int maxVal);
double clampDouble(double x, double minVal, double maxVal);
double squaredMagnitude(double complex x);

#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "v17.h"
#include "v21.h"
#include "t30.h"
#include "hdlc.h"
#include "params.h"
#include "util.h"
#include "log.h"

v17_demod_t v17;
v21_demod_t demod;
hdlc_decoder_t hdlc;
int v17Started, faxStarted, bitCounter;
unsigned char outByte;

// DCS: training.
int processDCS(unsigned char *fif, int len) {
    int dataRate, twoDimCoding, errorCorrection, errorLimiting;
    if (len < T30_DCS_MIN_LENGTH) {
        log_printf("[!] DCS FIF too short!");
        return 0;
    }

    dataRate = getBits(fif, 10, 14);
    twoDimCoding = getBit(fif, 15);
    errorCorrection = 0;
    errorLimiting = 0;
    if (getBit(fif, 16)) {
        if (len < T30_DCS_MIN_LENGTH + 1) {
            log_printf("[!] DCS FIF too short (but extend set to 1)\n");
            return 0;
        }
        errorCorrection = getBit(fif, 26);
        errorLimiting = getBit(fif, 27);
    }

    log_printf("-> DCS: dataRate=%d, twoDimCoding=%d, errorCorrection=%d, errorLimiting=%d\n", dataRate, twoDimCoding, errorCorrection, errorLimiting);

    if (dataRate != T30_DCS_RATE_V17_14K || twoDimCoding || errorCorrection || errorLimiting) {
        log_printf("[TESTING] This is not the expected mode!!!\n");
        return 0;
    }

    log_printf("Started V.17 acquisition.\n");

    v17Started = 1;
    v17_demod_startAcquisition(&v17, 0);

    return 1;
}

// CFR: fax starts!
int processCFR(unsigned char *fif, int len) {
    v17Started = 1;
    v17_demod_startAcquisition(&v17, 1);
    faxStarted = 1;
    return 1;
}

int processEOP(unsigned char *fif, int len) {
    outByte = 0;
    bitCounter = 0;
    v17Started = 0;
    faxStarted = 0;
    return 1;
}

int processEOM(unsigned char *fif, int len) {
    outByte = 0;
    bitCounter = 0;
    v17Started = 0;
    faxStarted = 0;
    return 1;
}

// Process an HDLC frame.
// Returns 1 on success, 0 on error.
int processFrame(unsigned char *data, int len) {
    int fcf, fcfGroup, fcfType, fifLength;
    unsigned char *fif;

    // Data is guaranteed to at least contain the FCF byte.
    fcf = data[2];
    fcfGroup = fcf >> 4;
    fcfType = fcf & 0xf;

    // The Facsimile Information Field.
    fif = data + 3;
    fifLength = len - 5;

    log_printf("FCF: %02x\n", fcf);
    switch (fcfGroup) {
        case T30_FCF_INIT:
            switch (fcfType) {
                case T30_INIT_DIS:
                    log_printf("-> T.30 DIS\n");
                    return 1;
                case T30_INIT_CSI:
                    log_printf("-> T.30 CSI\n");
                    return 1;
                default:
                    log_printf("-> T.30 init unimpl\n");
                    return 0;
            }
        case T30_FCF_RECEIVE_1:
        case T30_FCF_RECEIVE_2:
            switch (fcfType) {
                case T30_RECEIVE_DCS:
                    log_printf("-> T.30 DCS\n");
                    return processDCS(fif, fifLength);
                default:
                    log_printf("-> T.30 FCF receive unimpl\n");
                    return 0;
            }
        case T30_FCF_PREMSG_1:
        case T30_FCF_PREMSG_2:
            switch(fcfType) {
                case T30_PREMSG_CFR:
                    log_printf("-> T.30 CFR\n");
                    return processCFR(fif, fifLength);
                default:
                    log_printf("-> T.30 FCF premsg unimpl\n");
                    return 0;
            }
        case T30_FCF_POSTMSG_1:
        case T30_FCF_POSTMSG_2:
            switch(fcfType) {
                case T30_POSTMSG_EOP:
                    log_printf("-> T.30 EOP\n");
                    return processEOP(fif, fifLength);

                case T30_POSTMSG_EOM:
                    log_printf("-> T.30 EOM\n");
                    return processEOM(fif, fifLength);

                case T30_POSTMSG_MPS:
                    log_printf("-> T.30 MPS\n");

                    // To retrieve the second page we can call processCFR here!
                    return 0;
                default:
                    log_printf("-> T.30 FCF postmsg unimpl\n");
                    return 0;
            }

        default:
            log_printf("-> T.30 unimpl FCF!\n");
            return 0;
    }
}

int main(int argc, char *argv[]) {
    int i, ret, mixSamp;
    int16_t channels[2];
    FILE *faxFile;

    if (!v21_demod_alloc(&demod, V21_CHANNEL)) {
        perror("V.21 alloc failure");
        return EXIT_FAILURE;
    }

    if (!hdlc_decoder_alloc(&hdlc, HDLC_BUFSIZE)) {
        perror("HDLC alloc failure");
        v21_demod_free(&demod);
        return EXIT_FAILURE;
    }

    if (!v17_demod_alloc(&v17, V17_RATE_14K)) {
        perror("V.17 alloc failure");
        v21_demod_free(&demod);
        hdlc_decoder_free(&hdlc);
        return EXIT_FAILURE;
    }

    if (!(faxFile = fopen("debug/fax.g3", "w"))) {
        perror("Couldn't open output file");
        v21_demod_free(&demod);
        hdlc_decoder_free(&hdlc);
        v17_demod_free(&v17);
        return EXIT_FAILURE;
    }

    // Haven't started V.17 demod yet.
    v17Started = 0;
    faxStarted = 0;
    bitCounter = 0;
    outByte = 0;

    // Read samples.
    while (fread(&channels, sizeof(int16_t), 2, stdin)) {
        // Mix since we want HDLC from two channels.
        mixSamp = (int)channels[0] + channels[1];
        if ((ret = hdlc_decoder_input(&hdlc, v21_demod_input(&demod, mixSamp))) < 0) {
            //log_printf("HDLC error: %d\n", ret);
        } else if (ret > 0) {
            processFrame(hdlc.frame, ret);
        }

        // Feed the V.17 demodulator only if we started.
        if (v17Started && (ret = v17_demod_input(&v17, channels[1])) >= 0) {
            if (faxStarted) {
                // Shift in the bits.
                //for (i = v17.dataBitsPerSymbol - 1; i >= 0; i--) {
                for (i = 0; i < v17.dataBitsPerSymbol; i++) {
                    outByte <<= 1;
                    outByte |= (ret >> i) & 1;

                    // Byte, write it.
                    if (++bitCounter == 8) {
                        bitCounter = 0;
                        fwrite(&outByte, 1, 1, faxFile);
                        outByte = 0;
                    }
                }
            }
        }
    }

    v21_demod_free(&demod);
    hdlc_decoder_free(&hdlc);
    v17_demod_free(&v17);
    fflush(faxFile);
    fclose(faxFile);
    return EXIT_SUCCESS;
}

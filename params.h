#ifndef PARAMS_H
#define PARAMS_H

#define INPUT_RATE 8000

// No frames greater than this would go through.
#define HDLC_BUFSIZE 1024

// T.30 communications happen via V.21 channel #2.
#define V21_CHANNEL 1
#endif

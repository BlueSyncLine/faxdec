#include <math.h>
#include <complex.h>
#include <malloc.h>
#include <stdint.h>

#include "fir.h"
#include "constellation.h"
#include "v17.h"
#include "params.h"
#include "trellis-v17.h"
#include "util.h"
#include "log.h"
#include "nlms.h"

// Include the constellations.
#include "v17-constellations.h"

// V17 differential decoding table.
// First index: previous Y2Y1
// Second index: current Y2Y1
// Output: Q2Q1
const int V17_DIFF_DEC[4][4] = {
    {0, 1, 2, 3},
    {3, 0, 1, 2},
    {2, 3, 0, 1},
    {1, 2, 3, 0},
};

// Prepare for an acquisition.
void v17_demod_startAcquisition(v17_demod_t *demod, int retrain) {
    demod->retraining = retrain;
    demod->amplitudeLevel = 1.0;
    demod->loPhase = 0.0;
    demod->clockPhase = 0.0;
    demod->acquisitionStage = V17_ACQUISITION;
    demod->trainingIndex = 0;
    demod->preambleCounter = 0;
    demod->postambleCounter = 0;
    demod->syncPrev = 0;
    demod->syncPrevDelta = 0;
    demod->syncRun = 0;
    demod->gardnerStrobe = 0;
    demod->gardnerPrev = 0.0;
    demod->gardnerPrev2 = 0.0;
    demod->sampleCounter = 0;
    demod->lastSymbolTime = 0;
    fir_reset(&demod->resamplingFilter);
    v17_trellis_decoder_reset(&demod->trellis);

    // TODO this may depend on retrain?
    demod->diffPrev = 0;
    demod->descramblerState = 0;

    log_printf("v17: started acquisition, retrain=%d\n", retrain);
}

// Process a symbol.
int v17_demod_symbol(v17_demod_t *demod, double complex symbol) {
    int i, syncDelta, syncDeltaChanged, trellisOut, diffBits, diffOrig, dataBits, descrambled;
    double gardnerError;
    constellation_decision_t decision;

    // Debug output.
    printf("amplitude %f\n", demod->amplitudeLevel);

    // Clock recovery.
    if (cabs(demod->gardnerPrev2) < V17_GARDNER_MAX_AMPLITUDE && cabs(demod->gardnerPrev) < V17_GARDNER_MAX_AMPLITUDE && cabs(symbol) < V17_GARDNER_MAX_AMPLITUDE) {
        // TODO if the Gardner algorithm performs poorly there might be a need to only use opposite symbols.
        gardnerError = creal(demod->gardnerPrev) * (creal(symbol) - creal(demod->gardnerPrev2));
        gardnerError += cimag(demod->gardnerPrev) * (cimag(symbol) - cimag(demod->gardnerPrev2));
        demod->clockPhase += gardnerError * (demod->acquisitionStage == V17_ACQUISITION ? V17_CLOCK_COEFF_SYNC : V17_CLOCK_COEFF_DATA);
    }

    // Decide the current symbol.
    constellation_decide(demod->acquisitionStage == V17_DATA ? demod->dataConstellation : &V17_CONSTELLATION_SYNC, &decision, symbol);

    // Are we still synchronizing?
    switch (demod->acquisitionStage) {
        case V17_ACQUISITION:
            // Note: it doesn't matter which of the 90-degree quadrants we lock onto (a property of the V.17 constellation, see doi:10.1109/jsac.1984.1146102).
            demod->amplitudeLevel += V17_AGC_COEFF_SYNC * (demod->amplitudeLevel * decision.amplitudeError - demod->amplitudeLevel);
            demod->loPhase += decision.phaseError * V17_PHASE_COEFF_SYNC;
            //log_printf("v17 abab: phase error=%f, amplitude error=%f (amplitudeLevel=%f)\n", decision.phaseError, decision.amplitudeError, demod->amplitudeLevel);
            //log_printf("v17 abab: decision %d\n", decision.symbol.value);

            syncDelta = (decision.symbol.value - demod->syncPrev + 4) % 4;
            syncDeltaChanged = syncDelta != demod->syncPrevDelta;
            demod->syncPrevDelta = syncDelta;
            demod->syncPrev = decision.symbol.value;

            // If this isn't an ABAB alternation...
            if ((syncDelta != 1 && syncDelta != 3) || !syncDeltaChanged) {
                // We've been syncing long enough to assume that this is where training starts.
                if (demod->syncRun >= V17_SYNC_RUN_THRESHOLD) {
                    demod->acquisitionStage = V17_TRAINING;
                    log_printf("v17: acquired, starting training!\n");

                    // Copy over the resampling filter's taps...
                    for (i = 0; i < demod->resamplingFilter.numTaps; i++)
                        demod->equalizerTaps[i] = conj(demod->resamplingFilter.taps[i]);

                    // Skip the first training symbol.
                    demod->trainingIndex++;

                    // Done.
                    return -1;
                }

                // Reset the current run.
                demod->syncRun = 0;
            } else {
                // Increment the run duration.
                demod->syncRun++;
            }

            // No data.
            return -1;
         case V17_TRAINING:
            // Debug output.
            printf("training %f %f\n", creal(symbol), cimag(symbol));

            // Apply phase correction.
            demod->loPhase += decision.phaseError * V17_PHASE_COEFF_DATA;

            // Use the next training symbol.
            // Don't use the bridge signal.
            if (demod->trainingIndex < V17_LONG_TRAIN_LENGTH)
                nlms_step(&demod->resamplingFilter, demod->equalizerTaps, decision.symbol.coords * demod->amplitudeLevel, V17_ADAPTEQ_RATE);

            // Increment the index.
            demod->trainingIndex++;

            // Training symbols are over.
            if (demod->trainingIndex == (demod->retraining ? V17_SHORT_TRAIN_LENGTH : V17_LONG_TRAIN_LENGTH)) {
                log_printf("v17: training signal over\n");

                // Copy the equalizer taps back.
                for (i = 0; i < demod->resamplingFilter.numTaps; i++) {
                    demod->resamplingFilter.taps[i] = conj(demod->equalizerTaps[i]);
                    log_printf("%.12f+%.12fj, ", crealf(demod->resamplingFilter.taps[i]), cimagf(demod->resamplingFilter.taps[i]));
                }
                log_printf("\n");
            }

            // Bridge symbols don't count.
            if (demod->trainingIndex == (demod->retraining ? V17_SHORT_TRAIN_LENGTH : V17_LONG_TRAIN_LENGTH + V17_BRIDGE_LENGTH))
                demod->acquisitionStage = V17_DATA;

            // No data.
            return -1;
        case V17_DATA:
            // Debug output.
            printf("data %f %f %f %f\n", creal(symbol), cimag(symbol), decision.amplitudeError, decision.phaseError);

            // Special AGC.
            demod->amplitudeLevel += V17_AGC_COEFF_DATA * (demod->amplitudeLevel * decision.amplitudeError - demod->amplitudeLevel);

            // Apply phase correction.
            demod->loPhase += decision.phaseError * V17_PHASE_COEFF_DATA;

            // Feed it into the trellis decoder.
            // If it returns no data, there's nothing to do.
            if ((trellisOut = v17_trellis_decoder_input(&demod->trellis, &decision)) == -1)
                return -1;

            // Differential decoding.
            diffBits = (trellisOut >> 1) & 3;
            diffOrig = V17_DIFF_DEC[demod->diffPrev][diffBits];
            demod->diffPrev = diffBits;

            // Restore the data bits.
            dataBits = ((trellisOut >> 1) & ~3) | diffOrig;

            // Descramble.
            descrambled = 0;
            for (i = 0; i < demod->dataBitsPerSymbol; i++) {
                demod->descramblerState >>= 1;
                if ((dataBits >> i) & 1)
                    demod->descramblerState ^= V17_SCRAMBLER_POLY;

                descrambled |= (demod->descramblerState & 1) << demod->dataBitsPerSymbol;
                descrambled >>= 1;
            }

            // Skip the preamble.
            if (demod->preambleCounter < V17_PREAMBLE_LENGTH) {
                demod->preambleCounter++;
                return -1;
            }

            return descrambled;
    }

    return -1;
}

// Returns either -1 when no data is available or the data bits of a received symbol.
int v17_demod_input(v17_demod_t *demod, int16_t intSample) {
    int i, returnValue, symbolReturn;
    double complex symbol, sample, resampInput;

    returnValue = -1;

    // Convert to double.
    sample = (double)intSample / 32768.0;

    // Don't do anything if fed samples while the demod isn't ready in order to prevent accessing uninitialized memory.
    if (demod->acquisitionStage == V17_IDLE)
        return -1;

    for (i = 0; i < V17_UPSAMPLING_RATE; i++) {
        // Perform interpolation.
        if (i == 0) {
            resampInput = sample * cexp(-I * demod->loPhase * M_PI * 2);
            fir_feed(&demod->resamplingFilter, resampInput);
            demod->loPhase = fmod(demod->loPhase + V17_CARRIER_FREQUENCY / INPUT_RATE, 1.0);
        } else {
            // Repeat (as using zeros resulted in some filter taps being omitted when training).
            fir_feed(&demod->resamplingFilter, resampInput);
        }

        // Advance the clock.
        demod->clockPhase += V17_CLOCK_FREQUENCY / V17_RATE;

        // Sampling instant!
        if (demod->clockPhase >= 1.0) {
            // Symbol gap.
            printf("step %d\n", demod->sampleCounter - demod->lastSymbolTime);
            demod->lastSymbolTime = demod->sampleCounter;

            // Wrap the phase value around.
            demod->clockPhase = fmod(demod->clockPhase, 1.0);

            // Get the symbol out of the resampling filter.
            symbol = fir_mac(&demod->resamplingFilter) / demod->amplitudeLevel;

            // This is a correct symbol.
            if (demod->gardnerStrobe ^= 1) {
                // Feed it in.
                symbolReturn = v17_demod_symbol(demod, symbol);

                if (symbolReturn != -1) {
                    if (returnValue != -1)
                        log_printf("v17: overwriting returned bits, shouldn't happen!!!\n");

                    // Return the bits.
                    returnValue = symbolReturn;
                }
            }

            // Advance.
            demod->gardnerPrev2 = demod->gardnerPrev;
            demod->gardnerPrev = symbol;
        }
        demod->sampleCounter++;
    }

    return returnValue;
}

// Create a new V.17 demodulator.
int v17_demod_alloc(v17_demod_t *demod, int dataBitsPerSymbol) {
    int constellationType;

    demod->dataBitsPerSymbol = dataBitsPerSymbol;
    demod->acquisitionStage = V17_IDLE;

    // Choose the data constellation.
    switch (dataBitsPerSymbol) {
        case V17_RATE_14K:
            demod->dataConstellation = &V17_CONSTELLATION_DATA_128;
            constellationType = V17_TRELLIS_CONSTELLATION_128;
            break;
        case V17_RATE_12K:
            demod->dataConstellation = &V17_CONSTELLATION_DATA_64;
            constellationType = V17_TRELLIS_CONSTELLATION_64;
            break;
        case V17_RATE_9600:
            demod->dataConstellation = &V17_CONSTELLATION_DATA_32;
            constellationType = V17_TRELLIS_CONSTELLATION_32;
            break;
        case V17_RATE_7200:
            demod->dataConstellation = &V17_CONSTELLATION_DATA_16;
            constellationType = V17_TRELLIS_CONSTELLATION_16;
            break;
        default:
            return 0;
    }

    // Try creating the resampling filter (a root-raised-cosine one).
    if (!fir_rrc_alloc(&demod->resamplingFilter, V17_RATE, V17_BAUD_RATE, V17_RRC_BETA, V17_RESAMPLING_FILTER_N))
        return 0;

    // Try allocating the equalizer tap array.
    if (!(demod->equalizerTaps = calloc(demod->resamplingFilter.numTaps, sizeof(double complex)))) {
        fir_free(&demod->resamplingFilter);
        return 0;
    }

    // Try allocating the trellis decoder.
    if (!v17_trellis_decoder_alloc(&demod->trellis, demod->dataConstellation, constellationType)) {
        fir_free(&demod->resamplingFilter);
        free(demod->equalizerTaps);
        return 0;
    }

    // Success!
    return 1;
}

// Free the memory.
void v17_demod_free(v17_demod_t *demod) {
    fir_free(&demod->resamplingFilter);
    free(demod->equalizerTaps);
    v17_trellis_decoder_free(&demod->trellis);
}

#ifndef V21_H
#define V21_H
#include <complex.h>
#include "fir.h"

#define V21_BAUD_RATE 300.0
#define V21_DEVIATION 100.0
#define V21_CHANNEL_1_CARRIER 1080.0
#define V21_CHANNEL_2_CARRIER 1750.0
#define V21_FILTER_CUTOFF (V21_BAUD_RATE / 2 + V21_DEVIATION)

// Empirically chosen, the filter uses 2n + 1 taps.
#define V21_FILTER_N 32

// Clock recovery coefficient, set empirically.
#define V21_DEMOD_CLOCK_COEFF 0.3f

typedef struct {
    int channel;
    firfilt_t filt;
    double loPhase;
    double clockPhase;
    double complex filtPrev;
    int prevLevel;
} v21_demod_t;

int v21_demod_alloc(v21_demod_t *demod, int channel);
int v21_demod_input(v21_demod_t *demod, double sample);
void v21_demod_free(v21_demod_t *demod);
#endif

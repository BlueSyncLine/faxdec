#ifndef NLMS_H
#define NLMS_H
#include <complex.h>
#include "fir.h"

void nlms_step(firfilt_t *filter, double complex *taps, double complex expectedSymbol, double learningRate);
#endif

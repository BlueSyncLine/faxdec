#include <math.h>
#include <complex.h>
#include <malloc.h>
#include "constellation.h"
#include "trellis-v17.h"
#include "v17.h"
#include "log.h"
#include "util.h"

// Neighborhood coordinate deltas.
const int V17_TRELLIS_NEIGHBORS_16[9][2] = {
    {0, 0},

    {0, -4},
    {0, 4},
    {-4, 0},
    {4, 0},

    {-4, -4},
    {-4, 4},
    {4, -4},
    {4, 4}
};

const int V17_TRELLIS_NEIGHBORS_32[9][2] = {
    {0, 0},

    {-2, -2},
    {-2, 2},
    {2, -2},
    {2, 2},

    {0, -4},
    {0, 4},
    {-4, 0},
    {4, 0},
};

const int V17_TRELLIS_NEIGHBORS_64[9][2] = {
    {0, 0},

    {0, -2},
    {0, 2},
    {-2, 0},
    {2, 0},

    {-2, -2},
    {-2, 2},
    {2, -2},
    {2, 2}
};

const int V17_TRELLIS_NEIGHBORS_128[9][2] = {
    {0, 0},

    {-1, -1},
    {-1, 1},
    {1, -1},
    {1, 1},

    {0, -2},
    {0, 2},
    {-2, 0},
    {2, 0},
};

// Returns zero on a parity error.
int v17_trellis_parity(int currentState, int symbol) {
    return (symbol & 1) == (currentState & 1);
}

// Find the next state.
int v17_trellis_advance(int currentState, int symbol) {
    int a, b, c, aa, bb, cc, y1, y2, tap;
    // Unpack the bits.
    y2 = (symbol >> 2) & 1;
    y1 = (symbol >> 1) & 1;
    a = (currentState >> 2) & 1;
    b = (currentState >> 1) & 1;
    c = currentState & 1;

    // Advance (use separate variables because of interdependence).
    tap = b ^ y2;
    aa = c;
    bb = a ^ y1 ^ y2 ^ (c & tap);
    cc = tap ^ (c & y1);

    // Reassemble.
    return aa << 2 | bb << 1 | cc;
}

// Input a new symbol into the decoder.
// Returns either -1 when there are no bits or the bits of the constellation symbol when there are.
int v17_trellis_decoder_input(v17_trellis_decoder_t *decoder, const constellation_decision_t *decision) {
    int i, path, newState, nextIndex, tracebackIndex;
    double complex modifiedCoords;
    double newMetric, bestMetric;
    constellation_decision_t modifiedDecision;
    v17_trellis_entry_t *currentEntry, *nextEntry, *tracebackEntry, *tracebackBest;

    nextIndex = (decoder->currentIndex + 1) % V17_TRELLIS_TRUNCATION;

    // Allow discerning between next and overwritten symbols.
    for (i = 0; i < V17_TRELLIS_STATES; i++)
        decoder->trellis[nextIndex * V17_TRELLIS_STATES + i].symbol = -1;

    for (path = 0; path < V17_TRELLIS_STATES; path++) {
        currentEntry = &decoder->trellis[decoder->currentIndex * V17_TRELLIS_STATES + path];

        // Don't proceed with non-existent paths.
        if (currentEntry->symbol == -1 && decoder->skipCounter != V17_TRELLIS_TRUNCATION)
            continue;

        // Enumerate the 8-cell neighborhood.
        for (i = 0; i < 9; i++) {
            // Modify the coordinates.
            modifiedCoords = decision->symbol.coords + decoder->neighbors[i][0] + I*decoder->neighbors[i][1];

            // Not every modification is going to yield a valid point, verify.
            constellation_decide(decoder->constellation, &modifiedDecision, modifiedCoords);

            // Skip if no such symbol exists in the constellation.
            if (modifiedDecision.symbol.coords != modifiedCoords)
                continue;

            // New path.
            newState = v17_trellis_advance(path, modifiedDecision.symbol.value);
            nextEntry = &decoder->trellis[nextIndex * V17_TRELLIS_STATES + newState];

            // Tweak: use an IIR filter so that the metric doesn't overflow (mentioned in Texas Instruments' SPRA099).
            newMetric = currentEntry->pathMetric * 0.9 + squaredMagnitude(modifiedDecision.symbol.coords - decision->originalCoords) * 0.1;

            // Overwrite only if we won't introduce more errors that way.
            if (nextEntry->symbol == -1 || nextEntry->pathMetric > newMetric) {
                //if (nextEntry->symbol != -1)
                //    log_printf("v17 trellis: overwriting (error)\n");

                nextEntry->symbol = modifiedDecision.symbol.value;
                nextEntry->prevState = path;
                nextEntry->pathMetric = newMetric;
                break;
            }
        }

        // Assertion.
        if (i == 9)
            log_printf("v17 trellis: no neighbor chosen!\n");
    }

    // Update the current index.
    decoder->currentIndex = nextIndex;
    tracebackIndex = decoder->currentIndex;

    // If we're allowed to find output symbols right now, do traceback.
    if (!decoder->skipCounter) {
        // Find the best path.
        bestMetric = INFINITY;
        for (i = 0; i < V17_TRELLIS_STATES; i++) {
            tracebackEntry = &decoder->trellis[tracebackIndex * V17_TRELLIS_STATES + i];

            // Don't do traceback from non-existent paths!
            if (tracebackEntry->symbol == -1)
                continue;

            // Is this better than the others?
            if (bestMetric > tracebackEntry->pathMetric) {
                bestMetric = tracebackEntry->pathMetric;
                tracebackBest = tracebackEntry;
            }
        }

        printf("trellis %f\n", bestMetric);

        // Traceback.
        tracebackEntry = tracebackBest;
        for (i = 0; i < V17_TRELLIS_TRUNCATION - 1; i++) {
            tracebackIndex = (tracebackIndex - 1 + V17_TRELLIS_TRUNCATION) % V17_TRELLIS_TRUNCATION;
            tracebackEntry = &decoder->trellis[tracebackIndex * V17_TRELLIS_STATES + tracebackEntry->prevState];

            if (tracebackEntry->symbol == -1)
                log_printf("v17 trellis: this shouldn't happen. %d\n", i);
        }

        // Return the bits obtained from the symbol.
        return tracebackEntry->symbol;
    } else {
        decoder->skipCounter--;

        // No symbol to present at the moment..
        return -1;
    }
}

// Resets the trellis decoder.
void v17_trellis_decoder_reset(v17_trellis_decoder_t *decoder) {
    int i;
    decoder->currentIndex = 0;

    // This intentionally skips the first state.
    decoder->skipCounter = V17_TRELLIS_TRUNCATION;

    for (i = 0; i < V17_TRELLIS_STATES; i++) {
        decoder->trellis[i].prevState = -1;
        decoder->trellis[i].symbol = -1;
        decoder->trellis[i].pathMetric = 0.0;
    }
}

// Allocates memory for a trellis decoder.
// 1 is returned on success, 0 on failure.
int v17_trellis_decoder_alloc(v17_trellis_decoder_t *decoder, const constellation_t *constellation, int constellationType) {
    decoder->constellation = constellation;

    switch (constellationType) {
        case V17_TRELLIS_CONSTELLATION_16:
            decoder->neighbors = V17_TRELLIS_NEIGHBORS_16;
            break;
        case V17_TRELLIS_CONSTELLATION_32:
            decoder->neighbors = V17_TRELLIS_NEIGHBORS_32;
            break;
        case V17_TRELLIS_CONSTELLATION_64:
            decoder->neighbors = V17_TRELLIS_NEIGHBORS_64;
            break;
        case V17_TRELLIS_CONSTELLATION_128:
            decoder->neighbors = V17_TRELLIS_NEIGHBORS_128;
            break;
        default:
            return 0;
    }

    // Try allocating the table.
    if (!(decoder->trellis = calloc(V17_TRELLIS_TRUNCATION * V17_TRELLIS_STATES, sizeof(v17_trellis_entry_t))))
        return 0;

    // Reset the decoder (initialize variables, etc.)
    v17_trellis_decoder_reset(decoder);

    return 1;
}

// Free up the memory used.
void v17_trellis_decoder_free(v17_trellis_decoder_t *decoder) {
    free(decoder->trellis);
}

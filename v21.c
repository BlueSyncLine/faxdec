#include <math.h>
#include <complex.h>
#include "fir.h"
#include "params.h"
#include "v21.h"

// Initializes the V.21 demodulator.
int v21_demod_alloc(v21_demod_t *demod, int channel) {
    demod->channel = channel;
    demod->loPhase = 0.0;
    demod->clockPhase = 0.0;
    demod->filtPrev = 0.0;
    demod->prevLevel = 0;
    return fir_lowpass_alloc(&demod->filt, INPUT_RATE, V21_FILTER_CUTOFF, V21_FILTER_N);
}

// Input a sample into the demodulator.
// Returns -1 when there's no output bit, 0 or 1 otherwise.
int v21_demod_input(v21_demod_t *demod, double sample) {
    int level, edge;
    double carrier;
    double complex filtOut;

    // Carrier frequency.
    carrier = demod->channel ? V21_CHANNEL_2_CARRIER : V21_CHANNEL_1_CARRIER;

    // Feed a new sample into the filter.
    filtOut = fir_next(&demod->filt, cexp(-I * M_PI * 2.0 * demod->loPhase) * sample);

    // Advance the LO phase.
    //demod->loPhase += carrier / INPUT_RATE;
    demod->loPhase = fmod(demod->loPhase + carrier / INPUT_RATE, 1.0);

    // Zero is the higher frequency.
    level = carg(filtOut * conj(demod->filtPrev)) > 0.0 ? 0 : 1;

    // Update the previous value.
    demod->filtPrev = filtOut;

    // Pulse edge?
    edge = level ^ demod->prevLevel;

    // Update the previous level.
    demod->prevLevel = level;

    // Clock recovery.
    if (edge) {
        /*// The edge happened later than expected, advance.
        if (demod->clockPhase >= 0.5f)
            demod->clockPhase += V21_DEMOD_CLOCK_COEFF;

        // The edge happened earlier, retract.
        // Don't allow it to go too far, though.
        else if (demod->clockPhase >= -0.5f)
            demod->clockPhase -= V21_DEMOD_CLOCK_COEFF;*/
        demod->clockPhase += V21_DEMOD_CLOCK_COEFF * (0.5 - demod->clockPhase);
    }

    // Update the clock.
    demod->clockPhase += V21_BAUD_RATE / INPUT_RATE;

    // Clock rollover, return a bit!
    if (demod->clockPhase >= 1.0) {
        demod->clockPhase = fmod(demod->clockPhase, 1.0);
        return level;
    }

    // No bit on hand.
    return -1;
}

// Free the memory used.
void v21_demod_free(v21_demod_t *demod) {
    fir_free(&demod->filt);
}
